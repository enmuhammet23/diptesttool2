from flask import Flask, json, g, request, jsonify, json
from random import choice,randint

app = Flask(__name__)



@app.route("/evaluate", methods=["POST"])
def evaluate():
    json_data = json.loads(request.data)
    givenSentence = str(json_data['textarea']).split()
    w = choice(givenSentence)
    i = randint(0,len(givenSentence)-1)
    givenSentence.insert(i,w)
    result = {"text": " ".join(givenSentence)}
    response = app.response_class(
        response=json.dumps(result),
        status=200,
        mimetype='application/json'
    )
    return response
@app.route("/evaluateDouble", methods=["POST"])
def evaluate2():
    json_data = json.loads(request.data)
    givenSentence1 = str(json_data['textarea1']).split()
    w = choice(givenSentence1)
    i = randint(0,len(givenSentence1)-1)
    givenSentence1.insert(i,w)

    givenSentence2 = str(json_data['textarea2']).split()
    w = choice(givenSentence2)
    i = randint(0,len(givenSentence2)-1)
    givenSentence2.insert(i,w)

    
    result = {"first": " ".join(givenSentence1) + "\n" + " ".join(givenSentence2),"second":"Second Output" }
    response = app.response_class(
        response=json.dumps(result),
        status=200,
        mimetype='application/json'
    )
    return response

if __name__ == "__main__":
   app.run(host='0.0.0.0')


